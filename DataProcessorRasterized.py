# Generate PDF report for Polaris 6 DOF Spacecraft Simulator results.
import warnings
warnings.simplefilter(action='ignore', category=RuntimeWarning)

from csv import reader
import PIL
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
from matplotlib.lines import Line2D
import cartopy
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from matplotlib import rc
from mayavi import mlab
from tvtk.api import tvtk
from tvtk.tvtk_base import TVTKBase, vtk_color_trait
from tvtk.common import configure_input_data
from tvtk.tools import ivtk
from mpl_toolkits.mplot3d import Axes3D
from pylatex import Document, PageStyle, Head, MiniPage, Foot, LargeText, MediumText, LineBreak, simple_page_number, LongTable, MultiColumn, Section, Subsection, Command, Figure, NoEscape, Tabu, Center, Tabular, VerticalSpace, HorizontalSpace, StandAloneGraphic, NewPage, HugeText, Command, SubFigure
from pylatex.package import Package
from pylatex.utils import bold, NoEscape
from datetime import datetime
import os


def ParseSimDataFile(filepath) :
  sim_XML_filepath = ""
  num_satellites = 0
  monte_carlo_method = False
  monte_carlo_runs = 0
  end_of_header_line = 0
  try:
    with open(filepath, 'r') as config_file:
      csv_reader = reader(config_file)
      for line in csv_reader :
        if 'END OF HEADER' in line :
          break
        if 'Generated from XML Configuration File: ' in line :
          sim_XML_filepath = line[1]
        if 'Number of Satellites simulated: ' in line :
          num_satellites = line[1]
        if 'Monte Carlo Method is Enabled: ' in line :
          monte_carlo_method = line[1]
        if 'Number of Monte Carlo runs per simulated Satellite: ' in line :
          monte_carlo_runs = line[1]
        end_of_header_line = end_of_header_line + 1
  except FileNotFoundError as e:
    raise e
  return {'Simulation Config XML Filepath': sim_XML_filepath, 
          'Number of Satellites Simulated': num_satellites,
          'Monte Carlo Method is Enabled' : monte_carlo_method, 
          'Number of  Monte Carlo Runs'   : monte_carlo_runs,
          'End Of Header Line Number'     : end_of_header_line}

def ParseSatDataFile(filepath) :
  satellite_id = ""
  XML_config_filepath = ""
  operating_mode = ""
  monte_carlo_run_num = ""
  end_of_header_line = 0
  try:
    with open(filepath, 'r') as config_file:
      csv_reader = reader(config_file)
      for line in csv_reader :
        if 'END OF HEADER' in line :
          break
        if 'Satellite ID: ' in line :
          satellite_id = line[1]
        if 'Generated from XML Configuration File: ' in line :
          XML_config_filepath = line[1]          
        if 'Logged Satellite Simulation Operating Mode: ' in line :
          operating_mode = line[1]
        if 'Monte Carlo Simulation Run: ' in line :
          monte_carlo_run_num = line[1]
        end_of_header_line = end_of_header_line + 1
  except FileNotFoundError as e:
    raise e
  return {'Data Filepath'             : filepath,
          'Satellite ID'              : satellite_id, 
          'XML Config File'           : XML_config_filepath,
          'Operating Mode'            : operating_mode,
          'Monte Carlo Run Number'    : monte_carlo_run_num, 
          'End Of Header Line Number' : end_of_header_line,
          'Data Frame'                : pd.read_csv(filepath, header= end_of_header_line + 1)}

def SatelliteData(data_filepaths) :
  data_logs = []
  for filepath in data_filepaths :
    data_logs.append(ParseSatDataFile(filepath))
  return data_logs 




# Input Parameters
sim_config_filepath = r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_Simulation_Configuration.csv"
data_filepaths = [
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Nominal.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_1.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_2.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_3.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_4.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_5.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_6.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_7.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_8.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_9.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_10.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_11.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_12.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_13.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_14.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_15.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_16.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_17.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_18.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_19.csv",
r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 1_Monte Carlo_Run_20.csv"
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Nominal.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_1.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_2.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_3.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_4.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_5.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_6.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_7.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_8.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_9.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_10.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_11.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_12.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_13.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_14.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_15.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_16.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_17.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_18.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_19.csv",
# r"C:\Users\harri\OneDrive\Projects\Programming\Cpp\Polaris-Dev\lib\Polaris\output\polaris_example4_SC 2_Monte Carlo_Run_20.csv"
]




# Core Program
config_params = ParseSimDataFile(sim_config_filepath)
sim_config_df = pd.read_csv(sim_config_filepath, header= config_params['End Of Header Line Number'] + 1)
log = SatelliteData(data_filepaths)
log = sorted(log, key = lambda x: x['Operating Mode'], reverse=True)
log = sorted(log, key = lambda x: x['Satellite ID'])

data = {}

for x in log : 
    if x['Satellite ID'] not in data :
      data[x['Satellite ID']] = [x]
    else :
      data.setdefault(x['Satellite ID'], []).append(x)




logo_file = os.path.join(os.path.dirname(__file__), r'PolarisLogo.png')
rc('text', usetex=True)
rc('font', size=10)
rc('legend', fontsize=8)
rc('text.latex', preamble=r'\usepackage{lmodern}')

###############################################################################
###                                                                         ###
###          Document Simulation Configuration Title Page Section           ###
###                                                                         ###
###############################################################################
geometry_options = {"margin": "0.5in", "head": "0pt", "includehead": "true", "includefoot": "true"}
doc = Document(geometry_options=geometry_options, lmodern=True)
doc.packages.append(Package('placeins'))
doc.preamble.append(Command('usepackage', 'lmodern'))
doc.preamble.append(Command('usepackage', 'pdflscape '))

with doc.create(Figure(position='h!')) as logo:
  doc.append(VerticalSpace("10mm"))
  logo.add_image(logo_file, width=NoEscape(r'0.4\linewidth'))

doc.append(VerticalSpace("10mm"))

header = PageStyle("header")
with header.create(Foot("L")):
    now = datetime.now()
    header.append("Report Generated: " + now.strftime("%m/%d/%Y, %H:%M:%S"))
with header.create(Foot("R")):
    header.append(simple_page_number())
with header.create(Head("R")) as right_header:
  with right_header.create(MiniPage(width=NoEscape(r"0.15\textwidth"), pos='r')) as logo_wrapper :
    logo_wrapper.append(StandAloneGraphic(image_options="width=70px", filename=r'PolarisLogo.png'))

doc.preamble.append(header)
doc.change_document_style("header")

with doc.create(Center()) as centered:
    with centered.create(MiniPage(align='c')):
        centered.append(HugeText(bold("Simulation Report")))


###############################################################################
###                                                                         ###
###                     Simulation Configuration Table                      ###
###                                                                         ###
###############################################################################
doc.append(VerticalSpace("10mm"))
with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | l |')) as config_table:
        config_table.add_row((MultiColumn(2, align='c', data=MediumText(bold('Simulation Configuration'))),))
        config_table.add_hline()
        config_table.add_row("Simulation XML Configuration File", os.path.basename(config_params['Simulation Config XML Filepath']))
        config_table.add_hline()
        config_table.add_row("Satellites Simulated", config_params['Number of Satellites Simulated'])
        config_table.add_hline()
        config_table.add_row("Monte Carlo Method Enabled", bool(config_params['Monte Carlo Method is Enabled']))
        config_table.add_hline()
        config_table.add_row("Number of Monte Carlo Simulations per Satellite", config_params['Number of  Monte Carlo Runs'])
        config_table.add_hline()
        config_table.add_row("Simulation Start Time UTC", sim_config_df._get_value(0, 'UTC [Gregorian]'))
        config_table.add_hline()
        config_table.add_row("Simulation End Time UTC", sim_config_df['UTC [Gregorian]'].iat[-1])
        config_table.add_hline()   
        config_table.add_row("Simulation Run Time [s]", sim_config_df['Runtime [s]'].iat[-1])
        config_table.add_hline()                      


###############################################################################
###                                                                         ###
###                     Environment Model Table                             ###
###                                                                         ###
###############################################################################
doc.append(VerticalSpace("5mm"))

with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | l |')) as model_table:
        model_table.add_row((MultiColumn(2, align='c', data=MediumText(bold('Environmental Model Configuration'))),))
        model_table.add_hline()
        model_table.add_row("Earth Orientation Model", sim_config_df._get_value(0, 'Earth Orientation Model Name'))
        model_table.add_hline()
        model_table.add_row("Earth Atmospheric Density Model", sim_config_df._get_value(0, 'Earth Atmospheric Density Model Name'))
        model_table.add_hline()
        model_table.add_row("Earth Gravity Field Model", sim_config_df._get_value(0, 'Earth Gravity Field Model Name'))
        model_table.add_hline()
        model_table.add_row("Earth Magnetic Field Model", sim_config_df._get_value(0, 'Earth Magnetic Field Model Name'))
        model_table.add_hline()
        model_table.add_row("Planetary Positions Model", sim_config_df._get_value(0, 'Planetary Pertubrations Model Name'))
        model_table.add_hline()
        model_table.add_row("Solar Radiation Pressure Model", sim_config_df._get_value(0, 'Solar Radiation Model Name'))
        model_table.add_hline()

###############################################################################
###                                                                         ###
###                       Developer Statement                               ###
###                                                                         ###
###############################################################################
doc.append(VerticalSpace("70mm"))

with doc.create(Center()) as centered:
  doc.append("Generated as part of Polaris: https://harrisonhandley.gitlab.io/polaris")
  doc.append(VerticalSpace("2mm"))
  doc.append("Polaris source code directory: https://gitlab.com/HarrisonHandley/polaris")
  
  ##############################################################################
  ###                                                                        ###
  ###          Satellite Orbital ECEF Plot for all Satellite                 ###
  ###                                                                        ###
  ##############################################################################
  # create a figure window (and scene)
  fig = mlab.figure(size=(1200, 1200), bgcolor=(1,1,1))

  colors = [(0.890625, 0.10196, 0.1098), (0.596078, 0.30588, 0.63921)]
  temp_index = 0
  id_array = []
  for sat_id in data :
    mlab.plot3d(-data[sat_id][0]['Data Frame']['X Position ECEF(ITRF) [m]'], 
                -data[sat_id][0]['Data Frame']['Y Position ECEF(ITRF) [m]'], 
                data[sat_id][0]['Data Frame']['Z Position ECEF(ITRF) [m]'],
                color=colors[temp_index],
                tube_radius=40000,
                name="Trajectory")
    temp_index = temp_index + 1
    id_array.append(sat_id)         

  # load and map the texture
  img = tvtk.JPEGReader()
  img.file_name = "BlueEarthLarge.jpg"
  texture = tvtk.Texture(input_connection=img.output_port, interpolate=1)
  # (interpolate for a less raster appearance when zoomed in)

  # create the sphere source with a given radius and angular resolution
  sphere = tvtk.TexturedSphereSource(radius=6378136.3, theta_resolution=1024,
                                    phi_resolution=1024)

  # assemble rest of the pipeline, assign texture    
  sphere_mapper = tvtk.PolyDataMapper(input_connection=sphere.output_port)
  sphere_actor = tvtk.Actor(mapper=sphere_mapper, texture=texture)
  fig.scene.add_actor(sphere_actor)

  mlab.view(-180, 90, 30000000)
  mlab.savefig(filename='ecef_plot.png')
  colors = ["#E41A1C", "#984EA3"]
  im = plt.imread("ecef_plot.png")
  labels = []
  for i in id_array:
    labels.append("ID: " + i + " Trajectory")
  elements = [Line2D([0], [0], label = l, color = c) for l, c in zip(labels, colors[:4])]
  plt.imshow(im)
  plt.legend(handles = elements)
  plt.title("Orbital Trajectories in ECEF Reference Frame")  
  plt.margins(0,0)
  plt.gca().set_axis_off()

  with doc.create(Figure(position='h!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=500)
    plot.add_caption('Plotted nominal orbital trajectories of each simulated satellite in ECEF(ITRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))
  doc.append(VerticalSpace("60mm")) 

###############################################################################
###                                                                         ###
###                Simulation Iterations Plot and Table                     ###
###                                                                         ###
###############################################################################
plt.plot(sim_config_df['Iterations'], sim_config_df['Runtime [s]'].diff(), rasterized=True)
plt.ylim(0, (sim_config_df['Runtime [s]'].diff()).max()*1.25)
plt.xlim(0, sim_config_df['Iterations'].iat[-1])
plt.title("Time Step Size vs Iteration")
plt.xlabel("Simulation Iterations")
plt.ylabel("Time Step Size [s]")
plt.grid(axis='both', alpha=.3)
plt.gca().spines["top"].set_alpha(0.0)    
plt.gca().spines["bottom"].set_alpha(0.3)
plt.gca().spines["right"].set_alpha(0.0)    
plt.gca().spines["left"].set_alpha(0.3) 

with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of time step size for each iteration of the simulation.')
    plt.close()

with doc.create(Center()) as centered:
  with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Time Step Size Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [s]"), 
                     bold("Iteration Index")))
      table.add_hline()
      table.add_row("Maximum Time Step Size", \
                    (sim_config_df['Runtime [s]'].diff()).max(), \
                    (sim_config_df['Runtime [s]'].diff()).idxmax())
      table.add_hline()
      table.add_row("Minimum Time Step Size", \
                    (sim_config_df['Runtime [s]'].diff()).min(), \
                    (sim_config_df['Runtime [s]'].diff()).idxmin()),
      table.add_hline()
      table.add_row("Mean Time Step Size", (sim_config_df['Runtime [s]'].diff()).mean(), "-")
      table.add_hline()

  doc.append(Command('FloatBarrier'))
doc.append(NewPage()) 

###############################################################################
###                                                                         ###
###                Satellite Configuration Section and Table                ###
###                                                                         ###
###############################################################################
for sat_id in data :
  sat_df_max = data[sat_id][0]['Data Frame']
  sat_df_min = data[sat_id][0]['Data Frame']
  doc.append(NewPage())

  for x in data[sat_id]:
    sat_df_max = np.fmax(*sat_df_max.align(x['Data Frame']))
    sat_df_min = np.fmin(*sat_df_min.align(x['Data Frame']))

  with doc.create(Section("Simulation Results for Satellite: " + sat_id)):
    doc.append(VerticalSpace("5mm"))
    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c |')) as table:
          table.add_row((MultiColumn(2, align='c', data=MediumText(bold(sat_id + ' Configuration'))),))
          table.add_hline()
          table.add_row("Satellite ID", data[sat_id][0]['Satellite ID'])
          table.add_hline()
          table.add_row("XML Configuration File", os.path.basename(data[sat_id][0]['XML Config File']))
          table.add_hline()
          table.add_row("Number of Magnetorquer Actuators", 
                        int(data[sat_id][0]['Data Frame'].columns.str.contains('Magnetorquer').sum() / 5))
          table.add_hline()
          table.add_row("Number of Reaction Wheel Actuators", 
                        int(data[sat_id][0]['Data Frame'].columns.str.contains('Reaction Wheel').sum() / 4))
          table.add_hline()
          table.add_row("Number of Thruster Actuators", 
                        int(data[sat_id][0]['Data Frame'].columns.str.contains('Thruster').sum() / 8))
          table.add_hline()
  doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Orbital ECEF Plot                          ###
  ###                                                                        ###
  ##############################################################################
  # create a figure window (and scene)
  fig = mlab.figure(size=(1200, 1200), bgcolor=(1,1,1))

  mlab.plot3d(-data[sat_id][0]['Data Frame']['X Position ECEF(ITRF) [m]'], 
              -data[sat_id][0]['Data Frame']['Y Position ECEF(ITRF) [m]'], 
              data[sat_id][0]['Data Frame']['Z Position ECEF(ITRF) [m]'],
              color=(0.890625, 0.10196, 0.1098),
              tube_radius=40000,
              name="Trajectory")

  # load and map the texture
  img = tvtk.JPEGReader()
  img.file_name = "BlueEarthLarge.jpg"
  texture = tvtk.Texture(input_connection=img.output_port, interpolate=1)
  # (interpolate for a less raster appearance when zoomed in)

  # create the sphere source with a given radius and angular resolution
  sphere = tvtk.TexturedSphereSource(radius=6378136.3, theta_resolution=1024,
                                     phi_resolution=1024)

  # assemble rest of the pipeline, assign texture    
  sphere_mapper = tvtk.PolyDataMapper(input_connection=sphere.output_port)
  sphere_actor = tvtk.Actor(mapper=sphere_mapper, texture=texture)
  fig.scene.add_actor(sphere_actor)
  # repeat code from one of the examples linked to in the question, except for specifying facecolors:

  mlab.view(-180, 90, 30000000)
  mlab.savefig(filename='ecef_plot.png')
  
  im = plt.imread("ecef_plot.png")
  labels = ["Satellite Trajectory"]
  colors = ["#E41A1C"]
  elements = [Line2D([0], [0], label = l, color = c) for l, c in zip(labels, colors[:4])]
  plt.imshow(im)
  plt.legend(handles = elements)
  plt.title(sat_id + " Orbital Trajectory in ECEF Reference Frame")  
  plt.margins(0,0)
  plt.gca().set_axis_off()

  with doc.create(Figure(position='h!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=500)
    plot.add_caption("Plotted nominal orbital trajectory for satellite: " + sat_id + " in ECEF(ITRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))
  doc.append(NewPage())  
  

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Ground Track Plot                          ###
  ###                                                                        ###
  ##############################################################################
  # threshold = 90
  # idx_wrap = np.nonzero(np.abs(np.diff(data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]']*180/np.pi)) > threshold)[0]+1

  # lon_1 = data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]'][:idx_wrap]*180/np.pi
  # lat_1 = data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]'][:idx_wrap]*180/np.pi

  # lon_2 = data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]'][idx_wrap:]*180/np.pi
  # lat_2 = data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]'][idx_wrap:]*180/np.pi
  
  # idx_wrap_max = np.nonzero(np.abs(np.diff(data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]']*180/np.pi)) > threshold)[0]+1

  # lon_1_max = data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]'][:idx_wrap]*180/np.pi
  # lat_1_max = data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]'][:idx_wrap]*180/np.pi

  # lon_2_max = data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]'][idx_wrap:]*180/np.pi
  # lat_2_max = data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]'][idx_wrap:]*180/np.pi
  
  
  
  
  
  
  
  doc.append(Command('begin{landscape}'))
  ax = plt.axes(projection=ccrs.PlateCarree(), title="Satellite: " + sat_id + " Ground Track")
  ax.stock_img()
  ax.plot(data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]']*180/np.pi,
          data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]']*180/np.pi,
          linestyle='-', color='#E41A1C', transform=ccrs.PlateCarree(),
          label="Nominal Trajectory", rasterized=True)
  ax.fill_between(np.array(sat_df_max['Longitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
                  np.array(sat_df_max['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
                  np.array(sat_df_min['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
                  color="#E41A1C", alpha=0.35, transform=ccrs.PlateCarree())   
  ax.fill_between(np.array(sat_df_min['Longitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
                  np.array(sat_df_max['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
                  np.array(sat_df_min['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
                  color="#E41A1C", alpha=0.35, transform=ccrs.PlateCarree(),
                  label='Monte Carlo Distribution')
  ax.legend()                      
  gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                    linewidth=1, color='gray', alpha=1, linestyle='--')
  gl.top_labels = False
  gl.left_labels = False
  gl.xlines = False
  gl.xlocator = mticker.FixedLocator([-180, -90, 0, 90, 180])
  gl.ylocator = mticker.FixedLocator([-90, -45, 0, 45, 90])
  gl.xformatter = LONGITUDE_FORMATTER
  gl.yformatter = LATITUDE_FORMATTER
  
  

  
  
  
  # doc.append(Command('begin{landscape}'))
  # ax = plt.axes(projection=ccrs.PlateCarree(), title="Satellite: " + sat_id + " Ground Track")
  # ax.stock_img()
  # ax.plot(data[sat_id][0]['Data Frame']['Longitude WGS84(ITRF) [rad]']*180/np.pi,
  #         data[sat_id][0]['Data Frame']['Latitude WGS84(ITRF) [rad]']*180/np.pi,
  #         color='#E41A1C', transform=ccrs.PlateCarree(),
  #         label="Nominal Trajectory", zorder=-1, rasterized=True)
  # ax.fill_between(np.array(sat_df_max['Longitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
  #                 np.array(sat_df_max['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
  #                 np.array(sat_df_min['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
  #                 color="#E41A1C", alpha=0.35, transform=ccrs.PlateCarree(), zorder=-2)   
  # ax.fill_between(np.array(sat_df_min['Longitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi,
  #                 np.array(sat_df_max['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
  #                 np.array(sat_df_min['Latitude WGS84(ITRF) [rad]'], dtype=float)*180/np.pi, 
  #                 color="#E41A1C", alpha=0.35, transform=ccrs.PlateCarree(),
  #                 label='Monte Carlo Distribution', zorder=-3)
  # ax.set_rasterization_zorder(0)
  # ax.legend()                       
  # gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
  #                   linewidth=1, color='gray', alpha=1, linestyle='--')
  # gl.top_labels = False
  # gl.left_labels = False
  # gl.xlines = False
  # gl.xlocator = mticker.FixedLocator([-180, -90, 0, 90, 180])
  # gl.ylocator = mticker.FixedLocator([-90, -45, 0, 45, 90])
  # gl.xformatter = LONGITUDE_FORMATTER
  # gl.yformatter = LATITUDE_FORMATTER

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1.2\textwidth'), dpi=300)
    plot.add_caption("Plotted ground track for satellite: " + sat_id + " in WGS84 Lat/Lon reference frame.")
    plt.close()
    doc.append(Command('FloatBarrier'))
  doc.append(Command('end{landscape}'))


  ##############################################################################
  ###                                                                        ###
  ###             Satellite Orbital Eccentricity Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Eccentricity ECI(ICRF)', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital Eccentricity in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Eccentricity ECI(ICRF)'], dtype=float), 
                  np.array(sat_df_min['Eccentricity ECI(ICRF)'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Eccentricity")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.set_ylim(0)
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital eccentricity in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Eccentricity Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [Unitless]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Eccentricity ECI(ICRF)']) - \
                          pd.to_numeric(sat_df_min['Eccentricity ECI(ICRF)'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Eccentricity ECI(ICRF)'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Eccentricity ECI(ICRF)'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Eccentricity ECI(ICRF)']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Eccentricity ECI(ICRF)'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Eccentricity ECI(ICRF)'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Eccentricity ECI(ICRF)'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Eccentricity ECI(ICRF)']) - \
                          pd.to_numeric(sat_df_min['Eccentricity ECI(ICRF)'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Eccentricity ECI(ICRF)'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Eccentricity ECI(ICRF)'][min_delta_index], 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###           Satellite Orbital Semi-Major Axis Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Semi-Major Axis ECI(ICRF) [m]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital Semi-Major Axis in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Semi-Major Axis ECI(ICRF) [m]'], dtype=float), 
                  np.array(sat_df_min['Semi-Major Axis ECI(ICRF) [m]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Semi-Major Axis [m]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital semi-major axis in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Orbital Semi-Major Axis Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Semi-Major Axis ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Semi-Major Axis ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Semi-Major Axis ECI(ICRF) [m]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Semi-Major Axis ECI(ICRF) [m]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Semi-Major Axis ECI(ICRF) [m]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Semi-Major Axis ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Semi-Major Axis ECI(ICRF) [m]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Semi-Major Axis ECI(ICRF) [m]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Semi-Major Axis ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Semi-Major Axis ECI(ICRF) [m]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Semi-Major Axis ECI(ICRF) [m]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Semi-Major Axis ECI(ICRF) [m]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Orbital Inclination Plot and Table               ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Inclination ECI(ICRF) [deg]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital Inclination in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Inclination ECI(ICRF) [deg]'], dtype=float), 
                  np.array(sat_df_min['Inclination ECI(ICRF) [deg]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2) 
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Inclination [deg]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital inclination in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Orbital Inclination Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [deg]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Inclination ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Inclination ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Inclination ECI(ICRF) [deg]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Inclination ECI(ICRF) [deg]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Inclination ECI(ICRF) [deg]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Inclination ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Inclination ECI(ICRF) [deg]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Inclination ECI(ICRF) [deg]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Inclination ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Inclination ECI(ICRF) [deg]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Inclination ECI(ICRF) [deg]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Inclination ECI(ICRF) [deg]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###     Satellite Orbital Longitude of Ascending Node Plot and Table       ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Longitude of Ascending Node ECI(ICRF) [deg]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital Longitude of Ascending Node in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Longitude of Ascending Node ECI(ICRF) [deg]'], dtype=float), 
                  np.array(sat_df_min['Longitude of Ascending Node ECI(ICRF) [deg]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2) 
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Longitude of Ascending Node [deg]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital longitude of ascending node in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Orbital Longitude of Ascending Node Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [deg]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Longitude of Ascending Node ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Longitude of Ascending Node ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Longitude of Ascending Node ECI(ICRF) [deg]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Longitude of Ascending Node ECI(ICRF) [deg]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Longitude of Ascending Node ECI(ICRF) [deg]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Longitude of Ascending Node ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Longitude of Ascending Node ECI(ICRF) [deg]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Longitude of Ascending Node ECI(ICRF) [deg]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Longitude of Ascending Node ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Longitude of Ascending Node ECI(ICRF) [deg]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Longitude of Ascending Node ECI(ICRF) [deg]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Longitude of Ascending Node ECI(ICRF) [deg]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))        

  ##############################################################################
  ###                                                                        ###
  ###        Satellite Orbital Argument of Periapsis Plot and Table          ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Argument of Periapsis ECI(ICRF) [deg]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital Argument of Periapsis ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Argument of Periapsis ECI(ICRF) [deg]'], dtype=float), 
                  np.array(sat_df_min['Argument of Periapsis ECI(ICRF) [deg]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Argument of Periapsis [deg]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital argument of periapsis in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Orbital Argument of Periapsis Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [deg]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Argument of Periapsis ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Argument of Periapsis ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Argument of Periapsis ECI(ICRF) [deg]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Argument of Periapsis ECI(ICRF) [deg]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Argument of Periapsis ECI(ICRF) [deg]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Argument of Periapsis ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Argument of Periapsis ECI(ICRF) [deg]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Argument of Periapsis ECI(ICRF) [deg]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Argument of Periapsis ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['Argument of Periapsis ECI(ICRF) [deg]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Argument of Periapsis ECI(ICRF) [deg]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Argument of Periapsis ECI(ICRF) [deg]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()
      doc.append(VerticalSpace("20mm"))
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Orbital True Anomaly Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='True Anomaly ECI(ICRF) [deg]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Orbital True Anomaly in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['True Anomaly ECI(ICRF) [deg]'], dtype=float), 
                  np.array(sat_df_min['True Anomaly ECI(ICRF) [deg]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("True Anomaly [deg]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.set_ylim(0)
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellites orbital true anomaly in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Orbital True Anomaly Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [deg]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['True Anomaly ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['True Anomaly ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['True Anomaly ECI(ICRF) [deg]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['True Anomaly ECI(ICRF) [deg]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['True Anomaly ECI(ICRF) [deg]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['True Anomaly ECI(ICRF) [deg]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['True Anomaly ECI(ICRF) [deg]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['True Anomaly ECI(ICRF) [deg]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['True Anomaly ECI(ICRF) [deg]']) - \
                          pd.to_numeric(sat_df_min['True Anomaly ECI(ICRF) [deg]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['True Anomaly ECI(ICRF) [deg]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['True Anomaly ECI(ICRF) [deg]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Velocity Magnitude Plot and Table               ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Velocity Magnitude ECI(ICRF) [m/s]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Velocity Magnitude in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Velocity Magnitude ECI(ICRF) [m/s]'], dtype=float), 
                  np.array(sat_df_min['Velocity Magnitude ECI(ICRF) [m/s]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0) 
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Velocity Magnitude [m/s]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite velocity magnitude in ECI(ICRF).')
    plt.close()

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Velocity Magnitude Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Velocity Magnitude ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Velocity Magnitude ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Velocity Magnitude ECI(ICRF) [m/s]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Velocity Magnitude ECI(ICRF) [m/s]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Velocity Magnitude ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Velocity Magnitude ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Velocity Magnitude ECI(ICRF) [m/s]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Velocity Magnitude ECI(ICRF) [m/s]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Velocity Magnitude ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Velocity Magnitude ECI(ICRF) [m/s]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Velocity Magnitude ECI(ICRF) [m/s]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Velocity Magnitude ECI(ICRF) [m/s]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()             
      doc.append(Command('FloatBarrier'))  

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Acceleration Magnitude Plot and Table            ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Acceleration Magnitude ECI(ICRF) [m/s^2]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Acceleration Magnitude in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Acceleration Magnitude ECI(ICRF) [m/s^2]'], dtype=float), 
                  np.array(sat_df_min['Acceleration Magnitude ECI(ICRF) [m/s^2]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2) 
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Acceleration Magnitude [m/s\^{}2]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite acceleration magnitude ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Acceleration Magnitude Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s^2]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Acceleration Magnitude ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Acceleration Magnitude ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Acceleration Magnitude ECI(ICRF) [m/s^2]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Acceleration Magnitude ECI(ICRF) [m/s^2]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Acceleration Magnitude ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Acceleration Magnitude ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Acceleration Magnitude ECI(ICRF) [m/s^2]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Acceleration Magnitude ECI(ICRF) [m/s^2]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Acceleration Magnitude ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Acceleration Magnitude ECI(ICRF) [m/s^2]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Acceleration Magnitude ECI(ICRF) [m/s^2]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Acceleration Magnitude ECI(ICRF) [m/s^2]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()             
      doc.append(Command('FloatBarrier'))  

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Total Applied Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Total Applied Force ECI(ICRF) [N]', 
                                                              'Y Total Applied Force ECI(ICRF) [N]', 
                                                              'Z Total Applied Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Total Applied Force",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Total Applied Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-2)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-3) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-4)
  ax.set_rasterization_zorder(0)                                                                       
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Total Applied Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of total force applied to satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Total Applied Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Total Applied Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Total Applied Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Total Applied Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Total Applied Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Total Applied Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Total Applied Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Aerodynamic Drag Force Plot and Table           ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Aerodynamic Drag Force ECI(ICRF) [N]', 
                                                              'Y Aerodynamic Drag Force ECI(ICRF) [N]', 
                                                              'Z Aerodynamic Drag Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Aerodynamic Drag Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-2)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-3) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-4)         
  ax.set_rasterization_zorder(0)                                                                
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Aerodynamic Drag Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of aerodynamic drag force to satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Aerodynamic Drag Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Earth Gravity Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Earth Gravity Force ECI(ICRF) [N]', 
                                                              'Y Earth Gravity Force ECI(ICRF) [N]', 
                                                              'Z Earth Gravity Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Earth Gravity Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-2)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-3) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-4)  
  ax.set_rasterization_zorder(0)                                                                       
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Earth Gravity Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of Earth Gravity force to satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Earth Gravity Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Planetary Bodies Force Plot and Table           ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Planetary Bodies Force ECI(ICRF) [N]', 
                                                              'Y Planetary Bodies Force ECI(ICRF) [N]', 
                                                              'Z Planetary Bodies Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Planetary Bodies Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-2)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-3) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-4)
  ax.set_rasterization_zorder(0)                                                                      
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Planetary Bodies Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of Planetary Bodies force to satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Planetary Bodies Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier')) 

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Solar Pressure Force Plot and Table           ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Solar Pressure Force ECI(ICRF) [N]', 
                                                              'Y Solar Pressure Force ECI(ICRF) [N]', 
                                                              'Z Solar Pressure Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Solar Pressure Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-2)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-3) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-4)
  ax.set_rasterization_zorder(0)                                                                       
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Solar Pressure Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from Solar Radiation Pressure to satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Solar Pressure Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier')) 

  ##############################################################################
  ###                                                                        ###
  ###                 Satellite Propulsive Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Propulsive Force ECI(ICRF) [N]', 
                                                              'Y Propulsive Force ECI(ICRF) [N]', 
                                                              'Z Propulsive Force ECI(ICRF) [N]'], 
                                          color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                          label=["Nominal X Force", "Nominal Y Force", "Nominal Z Force"],
                                          title="Satellite: " + sat_id + " Propulsive Force in ECI(ICRF)",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Propulsive Force ECI(ICRF) [N]'], dtype=float),
                  color="#0066BB", alpha=0.35,
                  label='X Force Distribution',
                  zorder=-1)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'], dtype=float),
                  color="#E41A1C", alpha=0.35,
                  label='Y Force Distribution',
                  zorder=-2) 
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'], dtype=float),
                  color="#4DAF4A", alpha=0.35,
                  label='Z Force Distribution',
                  zorder=-3)
  ax.set_rasterization_zorder(0)                                                                     
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Propulsive Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of applied Propulsive Force to the satellite in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Propulsive Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      x_max_range_index = (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in X axis", \
                    (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]'][x_max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'][x_max_range_index])), \
                      x_max_range_index)
      table.add_hline()
      x_max_delta_index = (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                    (sat_df_max['X Propulsive Force ECI(ICRF) [N]'][x_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'][x_max_delta_index]), 
                    x_max_delta_index)
      table.add_hline()
      x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]']) - \
                            pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                    (sat_df_min['X Propulsive Force ECI(ICRF) [N]'][x_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'][x_min_delta_index]), 
                    x_min_delta_index)
      table.add_hline()

      y_max_range_index = (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                    (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]'][y_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'][y_max_range_index])), \
                      y_max_range_index)
      table.add_hline()
      y_max_delta_index = (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                    (sat_df_max['Y Propulsive Force ECI(ICRF) [N]'][y_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'][y_max_delta_index]), 
                    y_max_delta_index)
      table.add_hline()
      y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                    (sat_df_min['Y Propulsive Force ECI(ICRF) [N]'][y_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'][y_min_delta_index]), 
                    y_min_delta_index)
      table.add_hline()

      z_max_range_index = (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                    (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]'][z_max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'][z_max_range_index])), \
                      z_max_range_index)
      table.add_hline()
      z_max_delta_index = (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                    (sat_df_max['Z Propulsive Force ECI(ICRF) [N]'][z_max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'][z_max_delta_index]), 
                    z_max_delta_index)
      table.add_hline()
      z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                    (sat_df_min['Z Propulsive Force ECI(ICRF) [N]'][z_min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'][z_min_delta_index]), 
                    z_min_delta_index)
      table.add_hline()                     
      doc.append(Command('FloatBarrier')) 

  ##############################################################################
  ###                                                                        ###
  ###                       Satellite Mass Plot and Table                    ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Mass [kg]', 
                                          color="#0067BB",
                                          label="Mass",
                                          title="Satellite: " + sat_id + " Mass",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Mass [kg]'], dtype=float), 
                  np.array(sat_df_min['Mass [kg]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Mass [kg]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.set_ylim(0)
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of the satellites mass during the simulation")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Mass Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [kg]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Mass [kg]']) - \
                          pd.to_numeric(sat_df_min['Mass [kg]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Mass [kg]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Mass [kg]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Mass [kg]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Mass [kg]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Mass [kg]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Mass [kg]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Mass [kg]']) - \
                          pd.to_numeric(sat_df_min['Mass [kg]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Mass [kg]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Mass [kg]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier')) 

  # Skip all remaining plots if Attitude is not part of the satellite object.
  if (data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'] == 0).all() == 0 and \
     (data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'] == 0).all() == 0:

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Attitude Quaternion Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['Attitude Quaternion Scalar', 
                                                                'Attitude Quaternion Vector i', 
                                                                'Attitude Quaternion Vector j', 
                                                                'Attitude Quaternion Vector k'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A", "#984EA3"],
                                            label=["Nominal q0", "Nominal q1", "Nominal q2", "Nominal q3"],
                                            title="Satellite: " + sat_id + " Attitude Quaternion in Body-Fixed Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Scalar'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Scalar'], dtype=float),
                    color="#0066BB", alpha=0.4,
                    label='q0 Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector i'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector i'], dtype=float),
                    color="#E41A1C", alpha=0.4,
                    label='q1 Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector j'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector j'], dtype=float),
                    color="#4DAF4A", alpha=0.4,
                    label='q2 Distribution',
                    zorder=-4) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector k'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector k'], dtype=float),
                    color="#984EA3", alpha=0.4,
                    label='q3 Distribution',
                    zorder=-5)
    ax.set_rasterization_zorder(0)                                                                         
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Quaternion Value")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of the satellite attitude quaternion in Body-Fixed Reference Frame).")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Attitude Quaternion Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                        bold("Measured Value [Unitless]"), 
                        bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'])).abs().idxmax()
          table.add_row("q0 Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'])).abs().idxmax()
          table.add_row("q0 Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Attitude Quaternion Scalar'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'])).abs().idxmax()        
          table.add_row("q0 Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Attitude Quaternion Scalar'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()


          max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'])).abs().idxmax()
          table.add_row("q1 Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'])).abs().idxmax()
          table.add_row("q1 Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Attitude Quaternion Vector i'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'])).abs().idxmax()        
          table.add_row("q1 Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Attitude Quaternion Vector i'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()

          max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'])).abs().idxmax()
          table.add_row("q2 Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'])).abs().idxmax()
          table.add_row("q2 Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Attitude Quaternion Vector j'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'])).abs().idxmax()        
          table.add_row("q2 Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Attitude Quaternion Vector j'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'][min_delta_index]), 
                        min_delta_index)
          table.add_hline() 

          max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'])).abs().idxmax()
          table.add_row("q3 Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'])).abs().idxmax()
          table.add_row("q3 Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Attitude Quaternion Vector k'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'])).abs().idxmax()        
          table.add_row("q3 Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Attitude Quaternion Vector k'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()         
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###                 Satellite Angular Velocity Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Angular Velocity Body Frame [rad/s]', 
                                                                'Y Angular Velocity Body Frame [rad/s]', 
                                                                'Z Angular Velocity Body Frame [rad/s]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Angular Vel", "Nominal Y Angular Vel", "Nominal Z Angular Vel"],
                                            title="Satellite: " + sat_id + " Angular Velocity in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['X Angular Velocity Body Frame [rad/s]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Angular Vel Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['Y Angular Velocity Body Frame [rad/s]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Angular Vel Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['Z Angular Velocity Body Frame [rad/s]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Angular Vel Distribution',
                    zorder=-5)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Angular Velocity [rad/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite angular velocity in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Angular Velocity Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]']) - \
                              pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Angular Velocity Body Frame [rad/s]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]']) - \
                              pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Angular Velocity Body Frame [rad/s]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Angular Velocity Body Frame [rad/s]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Angular Velocity Body Frame [rad/s]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Angular Velocity Body Frame [rad/s]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Angular Velocity Body Frame [rad/s]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###                 Satellite Angular Acceleration Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Angular Acceleration Body Frame [rad/s^2]', 
                                                                'Y Angular Acceleration Body Frame [rad/s^2]', 
                                                                'Z Angular Acceleration Body Frame [rad/s^2]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Angular Acc", "Nominal Y Angular Acc", "Nominal Z Angular Acc"],
                                            title="Satellite: " + sat_id + " Angular Acceleration in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Angular Acc Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Angular Acc Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Angular Acc Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                   
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Angular Acceleration [rad/s\^{}2]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite angular acceleration in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Angular Acceleration Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s^2]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]']) - \
                              pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", 
                        (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'][x_max_range_index])),
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]']) - \
                              pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###            Total Applied Torque Body Frame Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Total Applied Torque Body Frame [Nm]', 
                                                                'Y Total Applied Torque Body Frame [Nm]', 
                                                                'Z Total Applied Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Applied Torque", "Nominal Y Applied Torque", "Nominal Z Applied Torque"],
                                            title="Satellite: " + sat_id + " Total Applied Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Total Applied Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Total Applied Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Total Applied Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                      
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Total Applied Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of total applied torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Total Applied Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Total Applied Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Total Applied Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Total Applied Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Total Applied Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Total Applied Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Total Applied Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier'))
		  
    ##############################################################################
    ###                                                                        ###
    ###            Aerodynamic Drag Torque Body Frame Plot and Table           ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Aerodynamic Drag Torque Body Frame [Nm]', 
                                                                'Y Aerodynamic Drag Torque Body Frame [Nm]', 
                                                                'Z Aerodynamic Drag Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Aerodynamic Drag Torque", "Nominal Y Aerodynamic Drag Torque", "Nominal Z Aerodynamic Drag Torque"],
                                            title="Satellite: " + sat_id + " Aerodynamic Drag Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                       
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Aerodynamic Drag Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Aerodynamic Drag torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Aerodynamic Drag Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 
		  
    ##############################################################################
    ###                                                                        ###
    ###            Earth Gravity Torque Body Frame Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Earth Gravity Torque Body Frame [Nm]', 
                                                                'Y Earth Gravity Torque Body Frame [Nm]', 
                                                                'Z Earth Gravity Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Earth Gravity Torque", "Nominal Y Earth Gravity Torque", "Nominal Z Earth Gravity Torque"],
                                            title="Satellite: " + sat_id + " Earth Gravity Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                         
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Earth Gravity Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Earth Gravity torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Earth Gravity Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Earth Gravity Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Earth Gravity Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 
		  
    ##############################################################################
    ###                                                                        ###
    ###            Magnetic Moment Torque Body Frame Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Magnetic Moment Torque Body Frame [Nm]', 
                                                                'Y Magnetic Moment Torque Body Frame [Nm]', 
                                                                'Z Magnetic Moment Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Magnetic Moment Torque", "Nominal Y Magnetic Moment Torque", "Nominal Z Magnetic Moment Torque"],
                                            title="Satellite: " + sat_id + " Magnetic Moment Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                        
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Magnetic Moment Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Magnetic Moment torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Magnetic Moment Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 
		  
    ##############################################################################
    ###                                                                        ###
    ###            Solar Pressure Torque Body Frame Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Solar Pressure Torque Body Frame [Nm]', 
                                                                'Y Solar Pressure Torque Body Frame [Nm]', 
                                                                'Z Solar Pressure Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Solar Pressure Torque", "Nominal Y Solar Pressure Torque", "Nominal Z Solar Pressure Torque"],
                                            title="Satellite: " + sat_id + " Solar Pressure Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                         
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Solar Pressure Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Solar Pressure torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Solar Pressure Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Solar Pressure Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Solar Pressure Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 
		  
    ##############################################################################
    ###                                                                        ###
    ###            Propulsive Torque Body Frame Plot and Table                 ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['X Propulsive Torque Body Frame [Nm]', 
                                                                'Y Propulsive Torque Body Frame [Nm]', 
                                                                'Z Propulsive Torque Body Frame [Nm]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Propulsive Torque", "Nominal Y Propulsive Torque", "Nominal Z Propulsive Torque"],
                                            title="Satellite: " + sat_id + " Propulsive Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Propulsive Torque Body Frame [Nm]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Torque Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Propulsive Torque Body Frame [Nm]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Torque Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Propulsive Torque Body Frame [Nm]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Torque Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                                         
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Propulsive Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Propulsive torque to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Propulsive Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['X Propulsive Torque Body Frame [Nm]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['X Propulsive Torque Body Frame [Nm]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Y Propulsive Torque Body Frame [Nm]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]']) - \
                              pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Y Propulsive Torque Body Frame [Nm]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Z Propulsive Torque Body Frame [Nm]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Z Propulsive Torque Body Frame [Nm]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

































































  ##############################################################################
  ###                                                                        ###
  ###                   Satellite X Position Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Position ECI(ICRF) [m]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Position in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Position ECI(ICRF) [m]'], dtype=float), 
                  np.array(sat_df_min['X Position ECI(ICRF) [m]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Position [m]")           
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite X position in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Position Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['X Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Position ECI(ICRF) [m]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Position ECI(ICRF) [m]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['X Position ECI(ICRF) [m]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Position ECI(ICRF) [m]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['X Position ECI(ICRF) [m]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['X Position ECI(ICRF) [m]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Position ECI(ICRF) [m]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()                             
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Y Position Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Position ECI(ICRF) [m]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Position in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Position ECI(ICRF) [m]'], dtype=float), 
                  np.array(sat_df_min['Y Position ECI(ICRF) [m]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Position [m]") 
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Y position in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Position Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Y Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Position ECI(ICRF) [m]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Position ECI(ICRF) [m]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Y Position ECI(ICRF) [m]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Y Position ECI(ICRF) [m]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Y Position ECI(ICRF) [m]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()             
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Z Position Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Position ECI(ICRF) [m]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Position in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Position ECI(ICRF) [m]'], dtype=float), 
                  np.array(sat_df_min['Z Position ECI(ICRF) [m]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Position [m]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Z position in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Position Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Z Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", 
                    (pd.to_numeric(sat_df_max['Z Position ECI(ICRF) [m]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Position ECI(ICRF) [m]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Y Position ECI(ICRF) [m]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Position ECI(ICRF) [m]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Position ECI(ICRF) [m]']) - \
                          pd.to_numeric(sat_df_min['Z Position ECI(ICRF) [m]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Z Position ECI(ICRF) [m]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Position ECI(ICRF) [m]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()             
      doc.append(Command('FloatBarrier'))



  ##############################################################################
  ###                                                                        ###
  ###                   Satellite X Velocity Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Velocity ECI(ICRF) [m/s]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Velocity in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  np.array(sat_df_min['X Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Velocity [m/s]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite X velocity in ECI(ICRF).')
    plt.close()

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Velocity Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['X Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Velocity ECI(ICRF) [m/s]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Velocity ECI(ICRF) [m/s]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['X Velocity ECI(ICRF) [m/s]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Velocity ECI(ICRF) [m/s]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['X Velocity ECI(ICRF) [m/s]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['X Velocity ECI(ICRF) [m/s]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Velocity ECI(ICRF) [m/s]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()             
      doc.append(Command('FloatBarrier'))  

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Y Velocity Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  doc.append(VerticalSpace("5mm"))

  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Velocity ECI(ICRF) [m/s]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Velocity in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  np.array(sat_df_min['Y Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Velocity [m/s]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Y velocity in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Velocity Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Y Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Velocity ECI(ICRF) [m/s]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Velocity ECI(ICRF) [m/s]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Y Velocity ECI(ICRF) [m/s]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Velocity ECI(ICRF) [m/s]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Y Velocity ECI(ICRF) [m/s]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Y Velocity ECI(ICRF) [m/s]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Velocity ECI(ICRF) [m/s]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()         
      doc.append(Command('FloatBarrier'))  

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Z Velocity Plot and Table                  ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Velocity ECI(ICRF) [m/s]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Velocity in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  np.array(sat_df_min['Z Velocity ECI(ICRF) [m/s]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                    
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Velocity [m/s]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Z velocity in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Velocity Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [s]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Z Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Velocity ECI(ICRF) [m/s]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Velocity ECI(ICRF) [m/s]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Velocity ECI(ICRF) [m/s]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    sat_df_max['Z Velocity ECI(ICRF) [m/s]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Velocity ECI(ICRF) [m/s]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Velocity ECI(ICRF) [m/s]']) - \
                          pd.to_numeric(sat_df_min['Z Velocity ECI(ICRF) [m/s]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    sat_df_min['Z Velocity ECI(ICRF) [m/s]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Velocity ECI(ICRF) [m/s]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()         
      doc.append(Command('FloatBarrier'))  


  ##############################################################################
  ###                                                                        ###
  ###                   Satellite X Acceleration Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Acceleration ECI(ICRF) [m/s^2]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + r" X Acceleration in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  np.array(sat_df_min['X Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                    
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Acceleration [m/s\^{}2]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite X acceleration in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Acceleration Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s^2]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['X Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Acceleration ECI(ICRF) [m/s^2]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Acceleration ECI(ICRF) [m/s^2]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Upper Deviation from Nominal", 
                    sat_df_max['X Acceleration ECI(ICRF) [m/s^2]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Acceleration ECI(ICRF) [m/s^2]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['X Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()        
      table.add_row(r"Maximum Lower Deviation from Nominal", 
                    sat_df_min['X Acceleration ECI(ICRF) [m/s^2]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Acceleration ECI(ICRF) [m/s^2]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Y Acceleration Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Acceleration ECI(ICRF) [m/s^2]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Acceleration ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  np.array(sat_df_min['Y Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Acceleration [m/s\^{}2]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Y acceleration in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Acceleration Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s^2]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Y Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Acceleration ECI(ICRF) [m/s^2]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Acceleration ECI(ICRF) [m/s^2]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Upper Deviation from Nominal", 
                    sat_df_max['Y Acceleration ECI(ICRF) [m/s^2]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Acceleration ECI(ICRF) [m/s^2]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Y Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()        
      table.add_row(r"Maximum Lower Deviation from Nominal", 
                    sat_df_min['Y Acceleration ECI(ICRF) [m/s^2]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Acceleration ECI(ICRF) [m/s^2]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###                   Satellite Z Acceleration Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Acceleration ECI(ICRF) [m/s^2]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Acceleration in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  np.array(sat_df_min['Z Acceleration ECI(ICRF) [m/s^2]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Acceleration [m/s\^{}2]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of satellite Z acceleration in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Acceleration Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [m/s^2]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Z Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Acceleration ECI(ICRF) [m/s^2]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Acceleration ECI(ICRF) [m/s^2]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()
      table.add_row(r"Maximum Upper Deviation from Nominal", 
                    sat_df_max['Z Acceleration ECI(ICRF) [m/s^2]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Acceleration ECI(ICRF) [m/s^2]'][max_delta_index], 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Acceleration ECI(ICRF) [m/s^2]']) - \
                          pd.to_numeric(sat_df_min['Z Acceleration ECI(ICRF) [m/s^2]'])).abs().idxmax()        
      table.add_row(r"Maximum Lower Deviation from Nominal", 
                    sat_df_min['Z Acceleration ECI(ICRF) [m/s^2]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Acceleration ECI(ICRF) [m/s^2]'][min_delta_index], 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Total Applied X Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Total Applied Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Total Applied Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                     
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Total Applied Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of total applied force to the satellite in the X axis in ECI(ICRF)")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Total Applied Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Total Applied Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Total Applied Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Total Applied Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))        

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Total Applied Y Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Total Applied Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Total Applied Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                     
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Total Applied Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of total applied force to the satellite in the Y axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Total Applied Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Total Applied Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Total Applied Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Total Applied Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###             Satellite Total Applied Z Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Total Applied Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Total Applied Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Total Applied Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of total applied force to the satellite in the Z axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Total Applied Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Total Applied Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Total Applied Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Total Applied Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Total Applied Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Aerodynamic Drag X Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Aerodynamic Drag Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Aerodynamic Drag Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Aerodynamic Drag Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of applied aerodynamic force to the satellite in X axis in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Aerodynamic Drag Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Aerodynamic Drag Y Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Aerodynamic Drag Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Aerodynamic Drag Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Aerodynamic Drag Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of applied aerodynamic force to the satellite in Y axis in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Aerodynamic Drag Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))      

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Aerodynamic Drag Z Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Aerodynamic Drag Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Aerodynamic Drag Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Aerodynamic Drag Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption('Plot of applied aerodynamic force to the satellite in Z axis in ECI(ICRF).')
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Aerodynamic Drag Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier')) 

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Earth Gravity X Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Earth Gravity Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Earth Gravity Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Earth Gravity Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of applied force from Earth's gravity to the satellite in X axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Earth Gravity Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Earth Gravity Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Earth Gravity Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Earth Gravity Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Earth Gravity Y Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Earth Gravity Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Earth Gravity Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Earth Gravity Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from Earth's gravity to the satellite in Y axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Earth Gravity Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Earth Gravity Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Earth Gravity Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Earth Gravity Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Earth Gravity Z Force Plot and Table              ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Earth Gravity Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Earth Gravity Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Earth Gravity Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from Earth's gravity to the satellite in Z axis in ECI(ICRF)")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Earth Gravity Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Earth Gravity Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Earth Gravity Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Earth Gravity Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Planetary Gravity X Force Plot and Table            ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Planetary Bodies Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Planetary Bodies Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Planetary Bodies Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption(r"Plot of applied force from planets gravity to the satellite in the X axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Planetary Bodies Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Planetary Gravity Y Force Plot and Table            ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Planetary Bodies Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Planetary Bodies Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                    
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Planetary Bodies Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from planets gravity to the satellite in the Y axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Planetary Bodies Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###          Satellite Planetary Gravity Z Force Plot and Table            ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Planetary Bodies Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Planetary Bodies Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                     
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Planetary Bodies Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from planets gravity to the satellite in the Z axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Planetary Bodies Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Planetary Bodies Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Solar Pressure X Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Solar Pressure Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Solar Pressure Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                  
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Solar Pressure Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from solar radiation pressure to the satellite in the X axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Solar Pressure Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))      
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Solar Pressure Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Solar Pressure Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Solar Pressure Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Solar Pressure Y Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Solar Pressure Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Solar Pressure Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Solar Pressure Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from solar radiation pressure to the satellite in the Y axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Solar Pressure Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Solar Pressure Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Solar Pressure Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Solar Pressure Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###            Satellite Solar Pressure Z Force Plot and Table             ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Solar Pressure Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Solar Pressure Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Solar Pressure Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from solar radiation pressure to the satellite in the Z axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Solar Pressure Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Solar Pressure Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Solar Pressure Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Solar Pressure Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Propulsive X Force Plot and Table               ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Propulsive Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " X Propulsive Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['X Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['X Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("X Propulsive Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from satellite thrusters in the X axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Propulsive Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['X Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['X Propulsive Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['X Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['X Propulsive Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['X Propulsive Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Propulsive Y Force Plot and Table               ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Propulsive Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Y Propulsive Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Y Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Y Propulsive Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from satellite thrusters in the Y axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Propulsive Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Y Propulsive Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Y Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Y Propulsive Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Y Propulsive Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))

  ##############################################################################
  ###                                                                        ###
  ###              Satellite Propulsive Z Force Plot and Table               ###
  ###                                                                        ###
  ##############################################################################
  ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Propulsive Force ECI(ICRF) [N]', 
                                          color="#0067BB",
                                          label="Nominal",
                                          title="Satellite: " + sat_id + " Z Propulsive Force in ECI Reference Frame",
                                          zorder=-1,
                                          rasterized=True)                                          
  ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                  np.array(sat_df_max['Z Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  np.array(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'], dtype=float), 
                  color="#0067BB", alpha=0.35,
                  label='Monte Carlo Distribution',
                  zorder=-2)
  ax.set_rasterization_zorder(0)                   
  ax.set_xlabel("Run Time [s]")
  ax.set_ylabel("Z Propulsive Force [N]")   
  ax.grid(axis='both', alpha=.3)
  ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
  ax.legend()
  ax.spines["top"].set_alpha(0.0)    
  ax.spines["bottom"].set_alpha(0.3)
  ax.spines["right"].set_alpha(0.0)    
  ax.spines["left"].set_alpha(0.3)

  with doc.create(Figure(position='t!')) as plot:
    plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
    plot.add_caption("Plot of applied force from satellite thrusters in the Z axis in ECI(ICRF).")
    plt.close()
    doc.append(Command('FloatBarrier'))

  with doc.create(Center()) as centered:
    with centered.create(Tabular('| l | c | c |',)) as table:
      table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Propulsive Force Parameters'))),))
      table.add_hline()
      table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                     bold("Measured Value [N]"), 
                     bold("Iteration Index")))
      table.add_hline()
      max_range_index = (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Monte Carlo Distribution", \
                    (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]'][max_range_index]) - \
                      pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'][max_range_index])), \
                      max_range_index)
      table.add_hline()
      max_delta_index = (pd.to_numeric(sat_df_max['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()
      table.add_row("Maximum Upper Deviation from Nominal", 
                    (sat_df_max['Z Propulsive Force ECI(ICRF) [N]'][max_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'][max_delta_index]), 
                    max_delta_index)
      table.add_hline()
      min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]']) - \
                          pd.to_numeric(sat_df_min['Z Propulsive Force ECI(ICRF) [N]'])).abs().idxmax()        
      table.add_row("Maximum Lower Deviation from Nominal", 
                    (sat_df_min['Z Propulsive Force ECI(ICRF) [N]'][min_delta_index] - \
                    data[sat_id][0]['Data Frame']['Z Propulsive Force ECI(ICRF) [N]'][min_delta_index]), 
                    min_delta_index)
      table.add_hline()        
      doc.append(Command('FloatBarrier'))


  # Skip all remaining plots if Attitude is not part of the satellite object.
  if (data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'] == 0).all() == 0 and \
     (data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'] == 0).all() == 0:


    ##############################################################################
    ###                                                                        ###
    ###                   Satellite Attitude Quaternion Scalar                 ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Attitude Quaternion Scalar', 
                                            color="#0067BB",
                                            label="Mass",
                                            title="Satellite: " + sat_id + " Attitude Quaternion q0 in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Scalar'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Scalar'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)                    
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Quaternion Value")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of the satellites q0 attitude quaternion in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
        table.add_row((MultiColumn(3, align='c', data=MediumText(bold('q0 Attitude Quaternion Parameters'))),))
        table.add_hline()
        table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                      bold("Measured Value [Unitless]"), 
                      bold("Iteration Index")))
        table.add_hline()
        max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'])).abs().idxmax()
        table.add_row("Maximum Monte Carlo Distribution", \
                      (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'][max_range_index])), \
                        max_range_index)
        table.add_hline()
        max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'])).abs().idxmax()
        table.add_row("Maximum Upper Deviation from Nominal", 
                      (sat_df_max['Attitude Quaternion Scalar'][max_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'][max_delta_index]), 
                      max_delta_index)
        table.add_hline()
        min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Scalar'])).abs().idxmax()        
        table.add_row("Maximum Lower Deviation from Nominal", 
                      (sat_df_min['Attitude Quaternion Scalar'][min_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Scalar'][min_delta_index]), 
                      min_delta_index)
        table.add_hline()        
        doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###                   Satellite Attitude Quaternion Vector i               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Attitude Quaternion Vector i', 
                                            color="#0067BB",
                                            label="Mass",
                                            title="Satellite: " + sat_id + " Attitude Quaternion q1 in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector i'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector i'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)                     
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Quaternion Value")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of the satellites q1 attitude quaternion in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
        table.add_row((MultiColumn(3, align='c', data=MediumText(bold('q1 Attitude Quaternion Parameters'))),))
        table.add_hline()
        table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                      bold("Measured Value [Unitless]"), 
                      bold("Iteration Index")))
        table.add_hline()
        max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'])).abs().idxmax()
        table.add_row("Maximum Monte Carlo Distribution", \
                      (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'][max_range_index])), \
                        max_range_index)
        table.add_hline()
        max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'])).abs().idxmax()
        table.add_row("Maximum Upper Deviation from Nominal", 
                      (sat_df_max['Attitude Quaternion Vector i'][max_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'][max_delta_index]), 
                      max_delta_index)
        table.add_hline()
        min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector i'])).abs().idxmax()        
        table.add_row("Maximum Lower Deviation from Nominal", 
                      (sat_df_min['Attitude Quaternion Vector i'][min_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector i'][min_delta_index]), 
                      min_delta_index)
        table.add_hline()        
        doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###                   Satellite Attitude Quaternion Vector j               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Attitude Quaternion Vector j', 
                                            color="#0067BB",
                                            label="Mass",
                                            title="Satellite: " + sat_id + " Attitude Quaternion q2 in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector j'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector j'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2) 
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Quaternion Value")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of the satellites q2 attitude quaternion in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
        table.add_row((MultiColumn(3, align='c', data=MediumText(bold('q2 Attitude Quaternion Parameters'))),))
        table.add_hline()
        table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                      bold("Measured Value [Unitless]"), 
                      bold("Iteration Index")))
        table.add_hline()
        max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'])).abs().idxmax()
        table.add_row("Maximum Monte Carlo Distribution", \
                      (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'][max_range_index])), \
                        max_range_index)
        table.add_hline()
        max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'])).abs().idxmax()
        table.add_row("Maximum Upper Deviation from Nominal", 
                      (sat_df_max['Attitude Quaternion Vector j'][max_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'][max_delta_index]), 
                      max_delta_index)
        table.add_hline()
        min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector j'])).abs().idxmax()        
        table.add_row("Maximum Lower Deviation from Nominal", 
                      (sat_df_min['Attitude Quaternion Vector j'][min_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector j'][min_delta_index]), 
                      min_delta_index)
        table.add_hline()        
        doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###                   Satellite Attitude Quaternion Vector k               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Attitude Quaternion Vector k', 
                                            color="#0067BB",
                                            label="Mass",
                                            title="Satellite: " + sat_id + " Attitude Quaternion q3 in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Attitude Quaternion Vector k'], dtype=float), 
                    np.array(sat_df_min['Attitude Quaternion Vector k'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)   
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Quaternion Value")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of the satellites q3 attitude quaternion in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
        table.add_row((MultiColumn(3, align='c', data=MediumText(bold('q3 Attitude Quaternion Parameters'))),))
        table.add_hline()
        table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                      bold("Measured Value [Unitless]"), 
                      bold("Iteration Index")))
        table.add_hline()
        max_range_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'])).abs().idxmax()
        table.add_row("Maximum Monte Carlo Distribution", \
                      (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'][max_range_index])), \
                        max_range_index)
        table.add_hline()
        max_delta_index = (pd.to_numeric(sat_df_max['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'])).abs().idxmax()
        table.add_row("Maximum Upper Deviation from Nominal", 
                      (sat_df_max['Attitude Quaternion Vector k'][max_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'][max_delta_index]), 
                      max_delta_index)
        table.add_hline()
        min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k']) - \
                            pd.to_numeric(sat_df_min['Attitude Quaternion Vector k'])).abs().idxmax()        
        table.add_row("Maximum Lower Deviation from Nominal", 
                      (sat_df_min['Attitude Quaternion Vector k'][min_delta_index] - \
                      data[sat_id][0]['Data Frame']['Attitude Quaternion Vector k'][min_delta_index]), 
                      min_delta_index)
        table.add_hline()        
        doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###              Satellite X Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Angular Velocity Body Frame [rad/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Angular Velocity in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['X Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Angular Velocity [rad/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite X axis angular velocity in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Angular Velocity Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s]"), 
                            bold("Iteration Index")))     
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Angular Velocity Body Frame [rad/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['X Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Angular Velocity Body Frame [rad/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Velocity Body Frame [rad/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Y Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Angular Velocity Body Frame [rad/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Angular Velocity in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)                                            
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['Y Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)   
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Angular Velocity [rad/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite Y axis angular velocity in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Angular Velocity Body Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Angular Velocity Body Frame [rad/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Angular Velocity Body Frame [rad/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Velocity Body Frame [rad/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Z Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Angular Velocity Body Frame [rad/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Angular Velocity in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    np.array(sat_df_min['Z Angular Velocity Body Frame [rad/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Angular Velocity [rad/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite Z axis angular velocity in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Angular Velocity Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution",
                        (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Angular Velocity Body Frame [rad/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Velocity Body Frame [rad/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Angular Velocity Body Frame [rad/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Velocity Body Frame [rad/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite X Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Angular Acceleration Body Frame [rad/s^2]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Angular Acceleration in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)  
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Angular Acceleration [rad/s\^{}2]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite X axis angular acceleration in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Angular Acceleration Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s^2]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Angular Acceleration Body Frame [rad/s^2]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Angular Acceleration Body Frame [rad/s^2]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Angular Acceleration Body Frame [rad/s^2]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Y Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Angular Acceleration Body Frame [rad/s^2]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Angular Acceleration in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)  
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Angular Acceleration [rad/s\^{}2]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite Y axis angular acceleration in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Angular Acceleration Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s^2]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Angular Acceleration Body Frame [rad/s^2]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Angular Acceleration Body Frame [rad/s^2]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Angular Acceleration Body Frame [rad/s^2]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Z Angular Velocity Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Angular Acceleration Body Frame [rad/s^2]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Angular Acceleration in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    np.array(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Angular Acceleration [rad/s\^{}2]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite Z axis angular acceleration in Body-Inertial Reference Frame")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Angular Acceleration Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [rad/s^2]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Angular Acceleration Body Frame [rad/s^2]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]']) - \
                            pd.to_numeric(sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Angular Acceleration Body Frame [rad/s^2]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Angular Acceleration Body Frame [rad/s^2]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###                 Satellite Angular Momentum Plot and Table              ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]', 
                                                                'Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]', 
                                                                'Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Angular Momentum", "Nominal Y Angular Momentum", "Nominal Z Angular Momentum"],
                                            title="Satellite: " + sat_id + " Angular Momentum in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Angular Momentum Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Angular Momentum Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Angular Momentum Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Spacecraft Angular Momentum [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite angular momentum in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Angular Momentum Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###              Satellite X Angular Momentum Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Spacecraft X Angular Momentum Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Angular Momentum in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Angular Momentum [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite X axis angular momentum in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Angular Momentum Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft X Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Y Angular Momentum Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Angular Momentum in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Angular Momentum [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite Y axis angular momentum in body frame in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Angular Momentum Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", 
                        (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Y Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Satellite Z Angular Momentum Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Angular Momentum in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)   
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Angular Momentum [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption(r"Plot of satellite Z axis angular momentum in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Angular Momentum Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Spacecraft Z Angular Momentum Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Applied Angular Momentum Delta Plot and Table             ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                                                'Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                                                'Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X Angular Momentum Delta", "Nominal Y Angular Momentum Delta", "Nominal Z Angular Momentum Delta"],
                                            title="Satellite: " + sat_id + " Applied Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X Angular Momentum Delta Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y Angular Momentum Delta Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z Angular Momentum Delta Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)                                                     
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Applied Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied angular Momentum Delta to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Applied Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###              Applied X Angular Momentum Delta Plot and Table           ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Applied X Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Applied Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Applied Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied X axis angular Momentum Delta to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Applied X Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied X Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Applied Y Angular Momentum Delta Plot and Table           ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Applied Y Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Applied Y Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Y axis angular Momentum Delta to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Applied Y Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Y Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###              Applied Z Angular Momentum Delta Plot and Table           ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Z Angular Momentum Delta ECI",
                                            title="Satellite: " + sat_id + " Applied Z Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Applied Z Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Z axis angular Momentum Delta to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Applied Z Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Applied Z Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###           Reaction Wheel Angular Momentum Delta Plot and Table         ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=['RW X Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                                                'RW Y Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                                                'RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'], 
                                            color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                            label=["Nominal X RW Angular Momentum Delta", "Nominal Y RW Angular Momentum Delta", "Nominal Z RW Angular Momentum Delta"],
                                            title="Satellite: " + sat_id + " Reaction Wheel Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#0066BB", alpha=0.35,
                    label='X RW Angular Momentum Delta Distribution',
                    zorder=-2)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#E41A1C", alpha=0.35,
                    label='Y RW Angular Momentum Delta Distribution',
                    zorder=-3) 
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float),
                    color="#4DAF4A", alpha=0.35,
                    label='Z RW Angular Momentum Delta Distribution',
                    zorder=-4)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("RW Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellites reaction wheel angular Momentum Delta in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('RW Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [kg*m^2/s]"), 
                            bold("Iteration Index")))
          table.add_hline()
          x_max_range_index = (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in X axis", \
                        (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_range_index])), \
                        x_max_range_index)
          table.add_hline()
          x_max_delta_index = (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in X axis", 
                        (sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_max_delta_index]), 
                        x_max_delta_index)
          table.add_hline()
          x_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                              pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in X axis", 
                        (sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][x_min_delta_index]), 
                        x_min_delta_index)
          table.add_hline()

          y_max_range_index = (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Y axis", \
                        (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_range_index])), \
                        y_max_range_index)
          table.add_hline()
          y_max_delta_index = (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Y axis", 
                        (sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_max_delta_index]), 
                        y_max_delta_index)
          table.add_hline()
          y_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Y axis", 
                        (sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][y_min_delta_index]), 
                        y_min_delta_index)
          table.add_hline()

          z_max_range_index = (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution in Z axis", \
                        (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_range_index])), \
                        z_max_range_index)
          table.add_hline()
          z_max_delta_index = (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal in Z axis", 
                        (sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_max_delta_index]), 
                        z_max_delta_index)
          table.add_hline()
          z_min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal in Z axis", 
                        (sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][z_min_delta_index]), 
                        z_min_delta_index)
          table.add_hline()                     
          doc.append(Command('FloatBarrier')) 

    ##############################################################################
    ###                                                                        ###
    ###         Reaction Wheel X Angular Momentum Delta Plot and Table         ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='RW X Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Reaction Wheel X Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("RW X Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite reaction wheel X axis angular Momentum Delta in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('RW X Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW X Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###         Reaction Wheel Y Angular Momentum Delta Plot and Table         ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='RW Y Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Reaction Wheel Y Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("RW Y Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite reaction wheel Y axis angular Momentum Delta in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('RW Y Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Y Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###         Reaction Wheel Z Angular Momentum Delta Plot and Table         ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='RW Z Angular Momentum Delta Body Frame [kg*m^2/s]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Reaction Wheel Z Angular Momentum Delta in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    np.array(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("RW Z Angular Momentum Delta [kg*m\^{}2/s]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of satellite reaction wheel Z axis angular Momentum Delta in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('RW Z Angular Momentum Delta Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [s]"), 
                            bold("Iteration Index")))      
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]']) - \
                            pd.to_numeric(sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['RW Z Angular Momentum Delta Body Frame [kg*m^2/s]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))


    ##############################################################################
    ###                                                                        ###
    ###            X Total Applied Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Total Applied Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Total Applied Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Total Applied Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of total applied torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Total Applied Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Total Applied Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Total Applied Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Total Applied Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Total Applied Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Total Applied Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Total Applied Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Total Applied Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of total applied torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Total Applied Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Total Applied Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Total Applied Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Total Applied Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Total Applied Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Total Applied Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Total Applied Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Total Applied Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Total Applied Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of total applied torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Total Applied Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Total Applied Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Total Applied Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Total Applied Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Total Applied Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ############################################################################
    ###                                                                      ###
    ###            X Aerodynamic Drag Torque Body Frame Plot and Table       ###
    ###                                                                      ###
    ############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Aerodynamic Drag Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Aerodynamic Drag Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Aerodynamic Drag Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Aerodynamic Drag torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Aerodynamic Drag Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Aerodynamic Drag Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Aerodynamic Drag Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Aerodynamic Drag Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Aerodynamic Drag Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Aerodynamic Drag torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Aerodynamic Drag Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Aerodynamic Drag Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Aerodynamic Drag Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Aerodynamic Drag Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Aerodynamic Drag Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Aerodynamic Drag torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Aerodynamic Drag Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Aerodynamic Drag Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))



    ##############################################################################
    ###                                                                        ###
    ###            X Earth Gravity Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Earth Gravity Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Earth Gravity Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Earth Gravity Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Earth Gravity torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Earth Gravity Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Earth Gravity Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Earth Gravity Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Earth Gravity Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Earth Gravity Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Earth Gravity Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Earth Gravity Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Earth Gravity Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Earth Gravity torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Earth Gravity Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Earth Gravity Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Earth Gravity Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Earth Gravity Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Earth Gravity Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Earth Gravity Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Earth Gravity Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Earth Gravity Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Earth Gravity torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Earth Gravity Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Earth Gravity Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Earth Gravity Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Earth Gravity Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))



    ##############################################################################
    ###                                                                        ###
    ###            X Magnetic Moment Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Magnetic Moment Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Magnetic Moment Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Magnetic Moment Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Magnetic Moment torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Magnetic Moment Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Magnetic Moment Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Magnetic Moment Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Magnetic Moment Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Magnetic Moment Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Magnetic Moment Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Magnetic Moment Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Magnetic Moment Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Magnetic Moment torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Magnetic Moment Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Magnetic Moment Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Magnetic Moment Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Magnetic Moment Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Magnetic Moment Torque Body Frame Plot and Table         ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Magnetic Moment Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Magnetic Moment Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Magnetic Moment Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Magnetic Moment torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Magnetic Moment Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Magnetic Moment Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Magnetic Moment Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Magnetic Moment Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))


    ##############################################################################
    ###                                                                        ###
    ###            X Solar Pressure Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Solar Pressure Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Solar Pressure Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Solar Pressure Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Solar Pressure torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Solar Pressure Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Solar Pressure Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Solar Pressure Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Solar Pressure Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Solar Pressure Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Solar Pressure Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Solar Pressure Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Solar Pressure Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Solar Pressure torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Solar Pressure Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Solar Pressure Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Solar Pressure Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Solar Pressure Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Solar Pressure Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Solar Pressure Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Solar Pressure Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Solar Pressure Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Solar Pressure torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Solar Pressure Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Solar Pressure Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Solar Pressure Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Solar Pressure Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))


    ##############################################################################
    ###                                                                        ###
    ###            X Propulsive Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='X Propulsive Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " X Propulsive Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['X Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['X Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0)
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("X Propulsive Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Propulsive torque in X axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('X Propulsive Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['X Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['X Propulsive Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['X Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['X Propulsive Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['X Propulsive Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Y Propulsive Torque Body Frame Plot and Table            ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Y Propulsive Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Y Propulsive Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Y Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Y Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Y Propulsive Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Propulsive torque in Y axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Y Propulsive Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Y Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Y Propulsive Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Y Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Y Propulsive Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Y Propulsive Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))

    ##############################################################################
    ###                                                                        ###
    ###            Z Propulsive Torque Body Frame Plot and Table               ###
    ###                                                                        ###
    ##############################################################################
    ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y='Z Propulsive Torque Body Frame [Nm]', 
                                            color="#0067BB",
                                            label="Nominal",
                                            title="Satellite: " + sat_id + " Z Propulsive Torque in Body-Inertial Reference Frame",
                                            zorder=-1,
                                            rasterized=True)
    ax.fill_between(data[sat_id][0]['Data Frame']['Runtime [s]'],
                    np.array(sat_df_max['Z Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    np.array(sat_df_min['Z Propulsive Torque Body Frame [Nm]'], dtype=float), 
                    color="#0067BB", alpha=0.35,
                    label='Monte Carlo Distribution',
                    zorder=-2)
    ax.set_rasterization_zorder(0) 
    ax.set_xlabel("Run Time [s]")
    ax.set_ylabel("Z Propulsive Torque [Nm]")   
    ax.grid(axis='both', alpha=.3)
    ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
    ax.legend()
    ax.spines["top"].set_alpha(0.0)    
    ax.spines["bottom"].set_alpha(0.3)
    ax.spines["right"].set_alpha(0.0)    
    ax.spines["left"].set_alpha(0.3)

    with doc.create(Figure(position='t!')) as plot:
      plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
      plot.add_caption("Plot of applied Propulsive torque in Z axis to satellite in body-inertial reference frame.")
      plt.close()
      doc.append(Command('FloatBarrier'))

    with doc.create(Center()) as centered:
      with centered.create(Tabular('| l | c | c |',)) as table:
          table.add_row((MultiColumn(3, align='c', data=MediumText(bold('Z Propulsive Torque Parameters'))),))
          table.add_hline()
          table.add_row((MultiColumn(1, align='|c|', data=bold("Parameters")), 
                            bold("Measured Value [Nm]"), 
                            bold("Iteration Index")))
          table.add_hline()
          max_range_index = (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Monte Carlo Distribution", \
                        (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]'][max_range_index]) - \
                        pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'][max_range_index])), \
                        max_range_index)
          table.add_hline()
          max_delta_index = (pd.to_numeric(sat_df_max['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()
          table.add_row("Maximum Upper Deviation from Nominal", 
                        (sat_df_max['Z Propulsive Torque Body Frame [Nm]'][max_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'][max_delta_index]), 
                        max_delta_index)
          table.add_hline()
          min_delta_index = (pd.to_numeric(data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]']) - \
                            pd.to_numeric(sat_df_min['Z Propulsive Torque Body Frame [Nm]'])).abs().idxmax()        
          table.add_row("Maximum Lower Deviation from Nominal", 
                        (sat_df_min['Z Propulsive Torque Body Frame [Nm]'][min_delta_index] - \
                        data[sat_id][0]['Data Frame']['Z Propulsive Torque Body Frame [Nm]'][min_delta_index]), 
                        min_delta_index)
          table.add_hline()        
          doc.append(Command('FloatBarrier'))


    magnetorquer_num = int(np.floor(data[sat_id][0]['Data Frame'].columns.str.contains('Magnetorquer').sum() / 5))
    reaction_wheel_num = int(np.floor(data[sat_id][0]['Data Frame'].columns.str.contains('Reaction Wheel').sum() / 4))
    thruster_num = int(np.floor(data[sat_id][0]['Data Frame'].columns.str.contains('Thruster').sum() / 8))

    for i in range(magnetorquer_num):
      state_header = "Magnetorquer " + str(i) + " State"
      x_header = "Magnetorquer " + str(i) + " X Magnetic Moment [Am^2]"
      y_header = "Magnetorquer " + str(i) + " Y Magnetic Moment [Am^2]"
      z_header = "Magnetorquer " + str(i) + " Z Magnetic Moment [Am^2]"
      operating_header = "Magnetorquer " + str(i) + " Operating Load [%]"

      ############################################################################
      ###                                                                      ###
      ###             Magnetorquer Magnetic Moment Plot and Table              ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[x_header,
                                                                  y_header,
                                                                  z_header], 
                                              color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                              label=["X Magnetic Moment", "Y Magnetic Moment", "Z Magnetic Moment"],
                                              title="Satellite: " + sat_id + " Magnetorquer " + str(i) + " Magnetic Moment in Body-Fixed Reference Frame",
                                              zorder=-1,
                                              rasterized=True)                                              
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Magnetic Moment [Am\^{}2]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Magnetorquer " + str(i) + " magnetic moment in body-fixed reference frame.")
        plt.close()
        doc.append(Command('FloatBarrier'))

      ############################################################################
      ###                                                                      ###
      ###           Magnetorquer Operating Load Plot and Table                 ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[operating_header], 
                                              color=["#0066BB"],
                                              label=["Operating Load"],
                                              title="Satellite: " + sat_id + " Magnetorquer " + str(i) + " Operating Load",
                                              zorder=-1,
                                              rasterized=True)                                              
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Operating Load [\%]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.set_ylim(0, 1.2)
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption(r"Plot of satellite Magnetorquer " + str(i) + " operating load percentage.")
        plt.close()
        doc.append(Command('FloatBarrier'))

    for i in range(reaction_wheel_num):
      state_header = "Reaction Wheel " + str(i) + " State"
      ang_vel_header = "Reaction Wheel " + str(i) + " Angular Velocity [rad/s]"
      ang_acc_header = "Reaction Wheel " + str(i) + " Angular Acceleration [rad/s^2]"
      torque_header = "Reaction Wheel " + str(i) + " Motor Torque [Nm]"

      ############################################################################
      ###                                                                      ###
      ###           Reaction Wheel Angular Velocity Plot and Table             ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[ang_vel_header], 
                                              color=["#0066BB"],
                                              label=["Angular Velocity"],
                                              title="Satellite: " + sat_id + " Reaction Wheel " + str(i) + " Angular Velocity about Axis of Spin",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Angular Velocity [rad/s]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Reaction Wheel " + str(i) + " angular velocity about wheels axis of spin.")
        plt.close()
        doc.append(Command('FloatBarrier'))

      ############################################################################
      ###                                                                      ###
      ###          Reaction Wheel Angular Acceleration Plot and Table          ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[ang_acc_header], 
                                              color=["#0066BB"],
                                              label=["Angular Acceleration"],
                                              title="Satellite: " + sat_id + " Reaction Wheel " + str(i) + " Angular Acceleration about Axis of Spin",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Angular Acceleration [rad/s\^{}2]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Reaction Wheel " + str(i) + " angular acceleration about wheels axis of spin.")
        plt.close()
        doc.append(Command('FloatBarrier'))

      ############################################################################
      ###                                                                      ###
      ###             Reaction Wheel Motor Torque Plot and Table               ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[torque_header], 
                                              color=["#0066BB"],
                                              label=["Motor Torque"],
                                              title="Satellite: " + sat_id + " Reaction Wheel " + str(i) + " Applied Motor Torque",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Applied Motor Torque [Nm]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption(r"Plot of satellite Reaction Wheel " + str(i) + " applied motor torque.")
        plt.close()
        doc.append(Command('FloatBarrier'))


    for i in range(thruster_num):
      throttle_header = "Thruster " + str(i) + " Throttle [%]"
      x_force_header = "Thruster " + str(i) + " X Force [N]"
      y_force_header = "Thruster " + str(i) + " Y Force [N]"
      z_force_header = "Thruster " + str(i) + " Z Force [N]"
      x_torque_header = "Thruster " + str(i) + " X Torque [Nm]"
      y_torque_header = "Thruster " + str(i) + " Y Torque [Nm]"
      z_torque_header = "Thruster " + str(i) + " Z Torque [Nm]"
      flowrate_header = "Thruster " + str(i) + " Mass Flow Rate [kg/s]"

      ############################################################################
      ###                                                                      ###
      ###                  Thruster Throttle Plot and Table                    ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[throttle_header], 
                                              color=["#0066BB"],
                                              label=["Thruster Throttle"],
                                              title="Satellite: " + sat_id + " Thruster " + str(i) + " Throttle",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Throttle [\%]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.set_ylim(0, 1.2)
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Thruster " + str(i) + " throttle percentage.")
        plt.close()
        doc.append(Command('FloatBarrier'))

      ############################################################################
      ###                                                                      ###
      ###                   Thruster Force Plot and Table                      ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[x_force_header,
                                                                  y_force_header,
                                                                  z_force_header], 
                                              color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                              label=["X Thruster Force", "Y Thruster Force", "Z Thruster Force"],
                                              title="Satellite: " + sat_id + " Thruster " + str(i) + " Force in Body-Fixed Reference Frame",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Thrust Force [N]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption(r"Plot of satellite Thruster " + str(i) + " thrust force in body-fixed reference frame.")
        plt.close()
        doc.append(Command('FloatBarrier'))

      ############################################################################
      ###                                                                      ###
      ###                   Thruster Torque Plot and Table                      ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[x_torque_header,
                                                                  y_torque_header,
                                                                  z_torque_header], 
                                              color=["#0066BB", "#E41A1C", "#4DAF4A"],
                                              label=["X Thruster Torque", "Y Thruster Torque", "Z Thruster Torque"],
                                              title="Satellite: " + sat_id + " Thruster " + str(i) + " Torque in Body-Fixed Reference Frame",
                                              zorder=-1,
                                              rasterized=True)
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Thrust Torque [Nm]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Thruster " + str(i) + " thrust torque in body-fixed reference frame.")
        plt.close()
        doc.append(Command('FloatBarrier'))      

      ############################################################################
      ###                                                                      ###
      ###               Thruster Mass Flow Rate Plot and Table                 ###
      ###                                                                      ###
      ############################################################################
      ax = data[sat_id][0]['Data Frame'].plot(x='Runtime [s]', y=[flowrate_header], 
                                              color=["#0066BB"],
                                              label=["Thruster Mass Flow Rate"],
                                              title="Satellite: " + sat_id + " Thruster " + str(i) + " Mass Flow Rate",
                                              zorder=-1,
                                              rasterized=True)                                              
      ax.set_rasterization_zorder(0) 
      ax.set_xlabel("Run Time [s]")
      ax.set_ylabel("Mass Flow Rate [kg/s]")   
      ax.grid(axis='both', alpha=.3)
      ax.set_xlim(0, data[sat_id][0]['Data Frame']['Runtime [s]'].iat[-1])
      ax.legend()
      ax.spines["top"].set_alpha(0.0)    
      ax.spines["bottom"].set_alpha(0.3)
      ax.spines["right"].set_alpha(0.0)    
      ax.spines["left"].set_alpha(0.3)

      with doc.create(Figure(position='t!')) as plot:
        plot.add_plot(width=NoEscape(r'1\textwidth'), dpi=300)
        plot.add_caption("Plot of satellite Thruster " + str(i) + " mass flow rate.")
        plt.close()
        doc.append(Command('FloatBarrier'))    

doc.generate_pdf("Polaris Report Small", clean_tex=False)
